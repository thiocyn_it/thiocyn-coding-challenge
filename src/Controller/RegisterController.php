<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as AbstractController;
use Symfony\Component\Routing\Annotation\Route as Route;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register")
     */
    public function indexAction()
    {
        return $this->render('register/index.twig');
    }
}