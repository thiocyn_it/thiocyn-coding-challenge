#Thiocyn Coding Challenge

## Introduction
This is a small task designed for a common Full Stack Developer. This task tests your basic knowledge 
in PHP 7.x, HTML, CSS, Template Engines (Twig), Symfony Framework, Git, API's, MVC Pattern and OOP.
Further steps are explained below.
## Requirements
The following requirements should be pre installed on your computer. If not please install it manually:
* composer
* yarn
* nodejs
* PHP min version 7.1
* PHP mods: ctype, iconv, json, pcre, session, simplexml, tokenizer
* or https://symfony.com/doc/current/reference/requirements.html
## Installation
First you need to get the source code:
```bash
$ git clone https://bitbucket.org/thiocyn_it/thiocyn-coding-challenge.git
```
Next step is to install the requirements:
```bash
$ cd [project_folder]
$ composer install
$ yarn install
```
This command builds the assets and need to be executed every time you make changes.
```bash
$ yarn encore dev
```
Run project:
```bash
$ cd public
$ php -S localhost:8000
```
The Application is now available via http://localhost:8000

##Task
This case was a real life issue we had in our company: If customer register they have to
select the salutation, mr or ms. We need this information for product information 
separated for male and female. User have the option for a fast checkout via paypal, so they 
dont have to submit the register form. Paypal sends us after the checkout process the user informations,
excluding the salutation. So each customer is stored with the standard salutation "mr", which leads to
that female users are getting the wrong product information. This case inspired us for this small task.  
Your job is to identify the sex of the user and inform him/her if its wrong, so he/she have
the opportunity for a last correction before submitting the form. You can
use different API's for checking the gender, like genderize.io.  
The register form: http://localhost:8000/register  
For this task you have the full power of symfony including bootstrap as frontend framework.
Feel free to use the wisdom of the internet.

